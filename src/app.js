import React, {useState} from "react";
import './styles.css';
import data from "./data";
import {makeRandomID} from "./data";


export function BuyList() {
    const [todoList, setTodoList] = useState(data)
    const handleAddList = () => {
        const title = prompt('please enter the title')
        const explain = prompt('please enter the explain')
        setTodoList([...todoList, {id: makeRandomID(), title, explain, item: []}])
    }

    const handleDeleteList = (id) => {
        setTodoList(todoList.filter(item => item.id !== id))
    }

    const handleAddItems = (id) => {
        const name = prompt('please enter the name')
        setTodoList(
            todoList.map((item) => (
                    item.id === id
                        ?
                        {...item, item: [...item.item, {id: makeRandomID(), name, status: false}]}
                        :
                        item
                )
            )
        )
    }
    const handleStatus = (dataId, itemsId, e) => {
        setTodoList(todoList.map(data => (
            data.id === dataId ? {
                ...data, item: data.item.map(items => items.id === itemsId ? {
                    ...items, status: e.target.checked
                } : items)
            } : data
        )))
    }
    return (
        <div className={'todo-list'}>
            {todoList.map(product => (
                <div className={'todo'} key={product.id}>
                    <h2>{product.title}</h2>
                    <button className={'delete-list'} onClick={() => handleDeleteList(product.id)}>Delete</button>
                    <h4>{product.explain}</h4>

                    {product.item.map(items => (
                        <ul key={`${product.id}${items.id}`}>
                            <li>
                                <input
                                    onChange={(e) => handleStatus(product.id, items.id, e)}
                                    type={'checkbox'}
                                    className={'checkbox'}
                                    checked={items.status}
                                    id={`${product.id}${items.id}`}
                                />
                                <label htmlFor={`${product.id}${items.id}`}>{items.name}</label>
                            </li>
                        </ul>
                    ))}
                    <button className={'add-item'} onClick={() => handleAddItems(product.id)}>
                        Add item
                    </button>
                </div>
            ))}
            <div>
                <button onClick={handleAddList} className={'creat-list'}>Creat list</button>
            </div>
        </div>
    )
}