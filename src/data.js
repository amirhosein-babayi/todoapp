const data = [
    {
        id: 1,
        title: 'Buy List For Bob',
        explain: 'These products are needed for the home',
        item: [
            {
                id: 1,
                name: 'milk',
                status: false
            },
            {
                id: 2,
                name: 'bread',
                status: false
            },
        ]
    },
    {
        id: 2,
        title: 'Buy List For Alice',
        explain: 'These products are needed for the home',
        item: [
            {
                id: 1,
                name: 'ice cream',
                status: false
            },
            {
                id: 2,
                name: 'cheese',
                status: false
            },
        ]
    }
]

export const makeRandomID = () => {
    return parseInt(Math.random() * 10000)
}
export default data